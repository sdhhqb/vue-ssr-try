import Vue from 'vue'
import App from './App.vue'

import { createRouter } from "./router";

export function createApp() {
  const router = createRouter()

  const app = new Vue({
    router,
    render: h => h(App)
  })
  return { app, router }
}

// const Vue = require('vue')
//
// module.exports = function createApp (context) {
//   return new Vue({
//     data: {
//       url: context.url
//     },
//     template: `<div>访问的url是： {{ url }}</div>`
//   })
// }