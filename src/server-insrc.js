const Vue = require('vue')
const server = require('express')()
const vueSSR = require('vue-server-renderer')

const createApp = require('./app')

const renderer = vueSSR.createRenderer()

server.get('*', (req, res) => {
  const context = { url: req.url }
  createApp(context).then(app => {
    renderer.renderToString(app, (err, html) => {
      if (err) {
        if (err.code === 404) {
          res.status(404).end('page not found')
        } else {
          res.status(500).end('internal server error')
        }
      } else {
        res.end(html)
      }
    })
  })
//   const app = createApp(context)
//
//   renderer.renderToString(app, (err, html) => {
//     res.end(`
// <!DOCTYPE html>
// <html>
//   <head>
//     <meta charset="UTF-8">
// </head>
//   <body>${html}</body>
// </html>
//     `)
//   })
})

server.listen(8000)
