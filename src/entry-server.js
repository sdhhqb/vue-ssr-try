import { createApp } from "./app";

export default context => {
  console.log('entry server call')

  return new Promise((resolve, reject) => {
    const { app, router } = createApp()

    router.push(context.url)
    router.onReady(() => {
      const matchedComponents = router.getMatchedComponents()

      if (!matchedComponents.length) {
        return reject({code: 404})
      }

      resolve(app)
    }, reject)

  })
}